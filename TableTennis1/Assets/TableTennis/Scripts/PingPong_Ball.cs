﻿using UnityEngine;
using System.Collections;

public class PingPong_Ball : MonoBehaviour {
	public bool reset = false;

	public Human_Player userBatScript;
	public AI_Player aiBatScript;

	public string batStatus;

	private float speed = 12;
	private Vector3 firstpostion;
	private bool firstServe;
	private Transform batTransform;
	private int groundCount;
	private string tableSideName;

    private Vector3 ballPos1; 
	void Start () {
		firstpostion = transform.position;
	}

	public void Reset()
	{
        //đưa bóng về vị trí ban đầu
		reset = false;

		GetComponent<Rigidbody>().useGravity = false;
		GetComponent<Rigidbody>().velocity = Vector3.zero;
		transform.position = firstpostion;
		aiBatScript.Reset();
		userBatScript.Reset();

	}
	
	
	public void Serve(Transform t)
	{
		reset = true;
        // đánh bóng theo hướng cong : đi thẳng + đi xuống
		GetComponent<Rigidbody>().useGravity = true;
		GetComponent<Rigidbody>().AddForce(Vector3.down*2.7f,ForceMode.Impulse);
		GetComponent<Rigidbody>().AddForce(transform.forward*speed,ForceMode.Impulse);

        // random số 1 hoặc 2, để xác định đi sang trái hoặc đi sang phải
		if(Random.Range(1,3) == 1)
			GetComponent<Rigidbody>().AddForce(transform.right*1.2f,ForceMode.Impulse);
		else
			GetComponent<Rigidbody>().AddForce(transform.right*-1.2f,ForceMode.Impulse);

		firstServe = true;  
		batTransform = t;
	}

    public void touch()
    {
        ballPos1 = transform.position;// lưu lại vị trí bóng lúc chạm vào bàn của mình
    }
	 void OnCollisionEnter(Collision collisionInfo) {
        // nếu quả bóng chạm vào bàn của người chơi thì lưu lại tên bàn 
		if(collisionInfo.collider.name == "UserSideTable")
		{
				userBatScript.RegisterSpin();   // lưu lại vị trí của vợt người chơi lúc bóng chạm vào bàn của mình
				tableSideName = "UserSideTable";
                touch();
            
		}
        // nếu quả bóng chạm vào bàn của máy mà đây là lần chạm đầu tiên
		if(collisionInfo.collider.name == "AiSideTable")
		{
			if(firstServe) 
			{
				batTransform.GetComponent<Collider>().isTrigger = false;
				batTransform.GetComponent<Rigidbody>().isKinematic =true;
				batTransform.GetComponent<Human_Player>().firstServe = false;
				firstServe = false;
			}

			tableSideName = "AiSideTable";
		}
        // nếu bóng ra ngoài
		if(collisionInfo.collider.name == "Wall")
		{
			groundCount++;
           // mà số lần ra ngoài là 2 thì reset lại
			if(groundCount == 2)
			{
				Reset();
				groundCount = 0;
                // nếu vợt của Máy chạm bóng
				if(batStatus == "abat")
				{ 
                    // nếu bàn cuối cùng bóng chạm vào là bàn người chơi thì AI đc điểm
					if(tableSideName == "UserSideTable"){
							AI_Player.points++;
					
					}
                    // ngược lại người chơi sẽ đc điểm
					if(tableSideName == "AiSideTable"){
							Human_Player.points++;
						
					}
				}

                // còn nếu vợt người chơi đánh bóng 
				if(batStatus == "ubat")
				{
                    // mà chạm bàn AI thì người chơi đc điểm
					if(tableSideName == "AiSideTable"){
							Human_Player.points++;

					}
					if(tableSideName == "UserSideTable"){
							AI_Player.points++;

					}
				}
			}
		}
	}
}
