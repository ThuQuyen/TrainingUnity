﻿using UnityEngine;
using System.Collections;

public class Human_Player : MonoBehaviour {
	
	private Vector3 pos1;
	private Vector3 pos2;
    private Vector3 ballPos2;
    private Vector3 ballPos1;

	public float speed;

	public float lastVelocity;

	public AI_Player aiBat;

	public Texture2D particleTexture;

	public Transform p;

	public bool firstServe = true;

	public Vector3 firstPostion;

	public static int points;

	public bool canMoveWithMouse;
    public PingPong_Ball ball;
    public Rigidbody rb;

	// Use this for initialization
	void Start () {
		points = 0;
		firstPostion = transform.position;
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		if(canMoveWithMouse) {
			transform.eulerAngles = new Vector3 (transform.eulerAngles.x,transform.eulerAngles.y,transform.position.x*-10); //= new  transform.position.x*-10;
			
			
			if(Application.platform == RuntimePlatform.IPhonePlayer)
			{
				foreach (Touch touch in Input.touches) 
				{
					transform.position = new Vector3(Mathf.Lerp(transform.position.x,transform.position.x+touch.deltaPosition.x * Time.deltaTime*2,0.2f),Mathf.Lerp(transform.position.y,transform.position.y+touch.deltaPosition.y * Time.deltaTime*2,0.2f),transform.position.z);
				}
			}
			else
			{
				transform.position = new Vector3(Mathf.Lerp(transform.position.x,transform.position.x+Input.GetAxis("Mouse X"),0.2f),Mathf.Lerp(transform.position.y,transform.position.y+Input.GetAxis("Mouse Y"),0.2f),transform.position.z);
			}
			
			// giới hạn vùng di chuyển của vợt	
			if(transform.position.x > 4)
				transform.position = new Vector3(4,transform.position.y,transform.position.z);
			if(transform.position.x < -4)
				transform.position = new Vector3(-4,transform.position.y,transform.position.z);
				
			if(transform.position.y < 1.2f)
				transform.position = new Vector3(transform.position.x,1.2f,transform.position.z);
			if(transform.position.y > 2.5)
				transform.position = new Vector3(transform.position.x,2.5f,transform.position.z);
		}
	}

	void OnCollisionEnter(Collision other)
	{
			
		if(other.collider.tag == "ball" && !firstServe)
		{
            pos1 = transform.position;    // vị trí vợt của người chơi chạm bóng
            ballPos2 = transform.position;// lưu lại vị trí bóng lúc chạm vợt mình

            // sự khác nhau  của vị trí vợt của người chơi lúc bóng chạm vào bàn người chơi và vị trí vợt người chơi chạm vào bóng
            float diff = (pos1.x - pos2.x);
            float diff2 = (ballPos2.z - ballPos1.z);
            // nếu 2 vị trí này càng # nhau thì độ xoáy càng lớn và ngược lại, xoáy theo trục X
			if(diff < -1)
			{
					diff = -1;
			}
			if(diff > 1)
			{
					diff = 1;
                    

			}
            //
       
       
            // người chơi đánh bóng thì biến String batStatus là ubat
			other.rigidbody.GetComponent<PingPong_Ball>().batStatus = "ubat";

            //để vợt ko bị tác động khi 2 vật va chạm, ta đưa vận tốc về 0 và cho chế độ isKinematic
			other.rigidbody.velocity = Vector3.zero;
			other.rigidbody.isKinematic = true;

            // add lực cho nó đi về phía trc.
			other.rigidbody.isKinematic = false;
			other.rigidbody.AddForce(transform.forward*15,ForceMode.Impulse);

            //tùy theo diff thì đánh bóng lệch trái - phải, nhiều - ít
			other.rigidbody.AddForce(transform.right*diff*2.5f,ForceMode.Impulse);

            //nếu để vợt quá thấp thì bóng sẽ ko qua lưới
			if(other.transform.position.y < 1.68f)
				other.rigidbody.AddForce(Vector3.up*1.6f,ForceMode.Impulse);
			else
				other.rigidbody.AddForce(Vector3.up*1.2f,ForceMode.Impulse);

			p.GetComponent<ParticleRenderer>().materials[0].mainTexture = particleTexture; 

			AI_Player.move = true;
			aiBat.HitDirection(diff);

		}
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Collider>().tag == "ball")
        {
            other.transform.SendMessage("Serve", transform);

        }

    }
    void OnCollistionEnter(Collision other)
    {
        if (other.collider.name=="ball")
        {
            ballPos1 = transform.position;
        }
    }

	public void RegisterSpin()
	{
        pos2 = transform.position;
	}

	public void Reset()
	{
        //mỗi lần đánh lại thì đưa vợt về vị trí ban đầu
        //và để chế độ sẵn sàng firsrServe lần đầu tiên chạm bóng
		if(canMoveWithMouse)
			transform.position = firstPostion;
		GetComponent<Collider>().isTrigger = true;
		GetComponent<Rigidbody>().isKinematic =false;
		firstServe = true;
	}
    //

}

